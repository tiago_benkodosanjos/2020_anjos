This is the mobile application that was started for Open Sesame. As of now it does not work as intended as there are many bugs. We ultimately decided not to use this application due to the visual bugs, concerns regarding the integration of the mobile application to our main AWS, lack of time, and lack of knowledge. It has been posted to show that there was progress made, but we could not finish it in time.

Currently the uploading image to AWS works, but there is a visual bug in the application which distorts the entire user interface.

A delete was attempted to be implemented but lack of knowledge prevented it from being functional, as it currently only deletes locally. However, since the application pulls from AWS the deleted person will reappear when the app is refreshed.

Planned on implementing a button to manually unlock door at a distance, but got stuck with bugs from the accepted persons list, and also lack of knowledge on how to correctly make an app caused a lot of time wasted and could not be implemented in time.

Did not connect this mobile application to the real AWS account as it handles some backend configurations when made and we were unsure how to do it correctly without corrupting our working project.

Overall this was intended for learning purposes and throughout its implementation a lot was learned and it helped get an idea of how to develop mobile applications. However, it ultimately came down to time and lack of resources online to learn how to create the mobile application we intended so we moved on to finishing the core concepts of this project.