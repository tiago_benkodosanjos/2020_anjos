package com.example.opensesame;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.amazonaws.amplify.generated.graphql.DeletePersonMutation;
import com.amazonaws.amplify.generated.graphql.ListPersonsQuery;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.appsync.fetcher.AppSyncResponseFetchers;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.DeleteItemRequest;
import com.apollographql.apollo.GraphQLCall;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

//Dynamo db imports
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.MenuInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

import javax.annotation.Nonnull;

import static com.example.opensesame.R.id.btn_signout;
import static com.example.opensesame.R.id.manual_lock;

public class MainActivity extends AppCompatActivity {

    RecyclerView mRecyclerView;
    MyAdapter mAdapter;

    private ArrayList<ListPersonsQuery.Item> mPersons;
    private final String TAG = MainActivity.class.getSimpleName();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerView = findViewById(R.id.recycler_view);

        // use a linear layout manager
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        // specify an adapter (see also next example)
        mAdapter = new MyAdapter(this);
        mRecyclerView.setAdapter(mAdapter);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(mRecyclerView);

        ClientFactory.init(this);


        FloatingActionButton btnAddPerson = findViewById(R.id.btn_addPerson);
        btnAddPerson.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent addPersonIntent = new Intent(MainActivity.this, AddPersonActivity.class);
                MainActivity.this.startActivity(addPersonIntent);
            }
        });
    }
    //CREATING MENU
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);
        return true;
    }
    //ON CLICK LISTENER FOR OPTION SELECTED
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()){

            case btn_signout:
                Toast.makeText(this, "Sign Out Selected", Toast.LENGTH_SHORT).show();
                AWSMobileClient.getInstance().signOut();
                Intent authIntent = new Intent(MainActivity.this, AuthenticationActivity.class);
                finish();
                startActivity(authIntent);
                return true;
            case manual_lock:
                Toast.makeText(this, "Manual Lock Selected", Toast.LENGTH_SHORT).show();
                return true;
            default: return super.onOptionsItemSelected(item);
        }

    }


    @Override
    public void onResume() {
        super.onResume();

        // Query list data when we return to the screen
        query();
    }

    public void query() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            Log.d(TAG, "WRITE_EXTERNAL_STORAGE permission not granted! Requesting...");
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    2);
        }
        ClientFactory.appSyncClient().query(ListPersonsQuery.builder().build())
                .responseFetcher(AppSyncResponseFetchers.CACHE_AND_NETWORK)
                .enqueue(queryCallback);
    }

    private GraphQLCall.Callback<ListPersonsQuery.Data> queryCallback = new GraphQLCall.Callback<ListPersonsQuery.Data>() {
        @Override
        public void onResponse(@Nonnull Response<ListPersonsQuery.Data> response) {

            mPersons = new ArrayList<>(response.data().listPersons().items());

            Log.i(TAG, "Retrieved list items: " + mPersons.toString());

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mAdapter.setItems(mPersons);
                    mAdapter.notifyDataSetChanged();
                }
            });
        }

        @Override
        public void onFailure(@Nonnull ApolloException e) {
            Log.e(TAG, e.toString());
        }
    };

    ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

            mPersons.remove(mAdapter.getPersonAt(viewHolder.getAdapterPosition()));
            mAdapter.notifyDataSetChanged();

        }
    };

}
